<?php

namespace App\Tests;

use FOPG\Component\UtilsBundle\Test\TestCase;
use FOPG\Component\UtilsBundle\Test\TestGiven;
use FOPG\Component\RpaBundle\Scraper\ChromeClient;

use App\Service\StructureScraperService;
use FOPG\Component\RpaBundle\Scraper\Scraper;

class ServiceStructureScraperTest extends TestCase
{
    public function testSomething(): void
    {   
        $serverUrl = 'http://fopg-selenium-chrome-instance:4444/wd/hub';
        $scraper = new StructureScraperService(new ChromeClient($serverUrl));
        
        $toto = $scraper->searchByCity("Paris");
        dump($toto);
        dump('toto' );die;
        $a = 5;
        $b = 3;
        $c = 7;
        $this
        ->given(
            description: 'Soit trois nombres a,b et c',
            a: $a,
            b: $b,
            c: $c
        )
        ->when(
            description: "J'applique l'opérateur > entre a et b",
            callback: function(int $a, int $b, &$opSup) { $opSup = ($a>$b); }
        )
        ->andWhen(
            description: "J'applique l'opérateur < entre a et c",
            callback: function(int $a, int $c, &$opInf) { $opInf = ($a<$c); }
        )
        ->then(
            description: "L'opérateur > doit être booléen et égal à true",
            callback: function($opSup){ return $opSup; },
            result: true,
            onFail: function(TestGiven $whoami) { $whoami->addError("le > n'est pas bon!", 101); }
        )
        ->andThen(
            description: "L'opérateur < doit être booléen et égal à true",
            callback: function($opInf){ return $opInf; },
            result: true,
            onFail: function(TestGiven $whoami) { $whoami->addError("le < n'est pas bon!", 101); }
        )
        ;



    }
}
